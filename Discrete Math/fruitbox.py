import math

print( "FRUITBOX PROBLEM" )

def C( n, r ):
    num = math.factorial( n )
    denom = math.factorial( r ) * math.factorial( n - r )
    return num/denom


fruitTypes = int( input( "How many different types of fruits? " ) )
separators = fruitTypes - 1

fruitCount = int( input( "How many total fruits to store in box? " ) )

totalBoxSpaces = fruitCount + separators

print( "--------------------------------" )
print( "Fruit types:      ", fruitTypes )
print( "Total separators: ", separators )
print( "Total fruits:     ", fruitCount )
print( "Total box spaces: ", totalBoxSpaces )
print( "--------------------------------" )

totalWays = C( totalBoxSpaces, fruitCount )
print( "Total ways to get fruits: ", totalWays )

totalWays = C( totalBoxSpaces, separators )
print( "Total ways to get fruits: ", totalWays )