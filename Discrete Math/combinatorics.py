import math

def P( n, r ):
    num = math.factorial( n )
    denom = math.factorial( n - r )
    return num/denom

def C( n, r ):
    num = math.factorial( n )
    denom = math.factorial( r ) * math.factorial( n - r )
    return num/denom


print( "DISCRETE MATH - COMBINATORICS!" )
type = input( "What kind of structure? (p/s/o/u/b): " )

if ( type == "p" ):
    n = int( input( "Enter your n value: " ) )
    r = int( input( "Enter your r value: " ) )
    # permutation: Order matters, no repeats
    permutation = P( n, r )
    print( "Permutation: ", permutation )

elif ( type == "s" ):
    n = int( input( "Enter your n value: " ) )
    r = int( input( "Enter your r value: " ) )
    # set: Order doesn't matter, no repeats
    mySet = C( n, r )
    print( "Set: ", mySet )

elif ( type == "o" ):
    n = int( input( "Enter your n value: " ) )
    r = int( input( "Enter your r value: " ) )
    # ordered list: Order matters, repeats allowed
    orderedList = math.pow( n, r )
    print( "Ordered list: ", orderedList )
    
elif ( type == "u" ):
    n = int( input( "Enter your n value: " ) )
    r = int( input( "Enter your r value: " ) )
    # unordered list: Order doesn't matters, repeats allowed    
    unorderedList = C( n, r )
    print( "Unordered list: ", unorderedList )
    
elif ( type == "b" ):
    # unordered list: Order doesn't matters, repeats allowed
    howManyOnes = int( input( "How many 1's? " ) )
    howManyZeroes = int( input( "How many 0's? " ) )
    r = howManyOnes
    n = howManyZeroes + r
    unorderedList = C( n, r )
    print( "Unordered list/binary string: ", unorderedList )