# Import the math library...
#from math import *      # pi
import math             # math.pi

print( "Circle Calculating Program" )
print( "" )

radius = float( input( "What is the radius of your circle? " ) )

# Calculate diameter
diameter = 2 * radius
print( "Diameter is", diameter )

# Calculate circumference
circumference = math.pi * diameter
print( "Circumference is", circumference )

# Calculate the area
area = math.pi * math.pow( radius, 2 ) # pi * r^2
print( "Area is", area )


print( "The square root of 4 is", math.sqrt( 4 ) )

